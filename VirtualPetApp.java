import java.util.Scanner;

public class VirtualPetApp {

    public static void main(String[] args) {
        Dog[] dogGang = new Dog[4];
        Scanner input = new Scanner(System.in);

        for (int i = 0; i < dogGang.length; i++) {
            System.out.println("Enter the name of the dog #" + (i + 1) + ": ");
            String name = input.nextLine();

            System.out.println("Enter the age of the dog: ");
            int age = input.nextInt();

            input.nextLine();

            System.out.println("Enter the weight of the dog: ");
            double weight = input.nextDouble();

            input.nextLine();

            System.out.println("Enter the favorite foods of the dog (separated by comma): ");
            String favoriteFoodsString = input.nextLine();
            String[] favoriteFoods = favoriteFoodsString.split(",");

            dogGang[i] = new Dog(name, age, weight, favoriteFoods);

            if (i == 0) {
                // bark
                dogGang[i].bark();

                // print its age in Human Years
                dogGang[i].getHumanYears();

                // list its favorite foods.
                dogGang[i].listFavoriteFoods();
            }
        }

        input.close();

        Dog lastDog = dogGang[dogGang.length - 1];

        // bark
        lastDog.bark();

        // print its age in Human Years
        lastDog.getHumanYears();

        // list its favorite foods.
        lastDog.listFavoriteFoods();

    }

}