public class Dog {

    public String name;
    public int age;
    public double weight;
    public String[] favoriteFood;

    public Dog (String name, int age, double weight, String[] favoriteFood){
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.favoriteFood = favoriteFood;
    }

    public void bark(){
        System.out.println("My name is " + name + ", I love barking.");
    }

    public void getHumanYears(){
        int ageHuman = age * 7;
        System.out.println("My age in human years is: " + ageHuman);
    }

    public void listFavoriteFoods(){
        String favoriteFoodStr = "";

        for (int i = 0; i < favoriteFood.length; i++){
            favoriteFoodStr += favoriteFood[i] + " ";
        }

        System.out.println("My Favorite Foods are: " + favoriteFoodStr);
    }


}
